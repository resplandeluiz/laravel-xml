<?php

namespace App\Http\Controllers;


use App\User;
use Session;
use Illuminate\Http\Request;
use Redirect;
use Validator;
use App\Imports\UsersImport;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;


class ImportController extends Controller
{

    private $validationUserForm;

    public function setValidate()
    {

        $this->validationUserForm = [
            'xlsx' => 'required|file|mimes:xlsx,xls,csv'
        ];
    }

    public function getValidate()
    {
        return $this->validationUserForm;
    }

    public function index()
    {
        return view('import.index');
    }

    public function updateData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'file' => 'required|file|mimes:xlsx,xml'
        ]);

        if (!$validator->fails()) {
            $data = $request->file('file');
            $ext = $data->clientExtension();

            if($ext == "xlsx"){
                Excel::import(new UsersImport, $data->getRealPath());
            }else if($ext == "xml"){
                $xml = simplexml_load_string($request->file('file')->get());
                foreach($xml as $xm){
                    $array =  (array) $xm;
                    (new UsersImport)->updateList(reset($array));
                }

            }

            Session::flash('message', 'Terminado com sucesso!');
            return Redirect::to('/users');
        } else {
            Session::flash('error', 'Problema ao enviar o formulário');
            return Redirect::back();
        }


    }


}
