<?php

namespace App\Imports;

use App\User;
use Maatwebsite\Excel\Concerns\ToModel;
use Validator;
use Session;

class UsersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $validator = Validator::make($row, [
            0 => 'required',
            1 =>'required|unique:users,documento'
        ]);

        if(!$validator->fails()){
            return new User([
                'nome'          => $row[0],
                'documento'     => $row[1],
                'cep'           => $row[2],
                'endereco'      => $row[3],
                'bairro'        => $row[4],
                'cidade'        => $row[5],
                'uf'            => $row[6],
                'telefone'      => $row[7],
                'email'         => $row[8],
                'ativo'         => $row[9],
            ]);
        }

    }

    public function updateList($lista){

        $validator = Validator::make($lista, [
            'nome'=> 'required',
            'documento' =>'required|unique:users,documento',
            'email' => 'required|unique:users,email'
        ]);

        if(!$validator->fails()){
            $user = new User($lista);
            $user->save();
            return true;
        }

    }
}
