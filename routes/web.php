<?php




Route::get('/', 'ImportController@index')->name('home');
Route::post('/importData', 'ImportController@updateData');
Route::resource('/users', 'UserController');