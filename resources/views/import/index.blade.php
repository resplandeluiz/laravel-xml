@extends('template.template')

@section('content')
    <div class="col-4 offset-4 mt-5">
        <form action="/importData" method="POST" class="form-signin" enctype="multipart/form-data">
            @csrf

            <img class="mb-4"
                 src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Microsoft_Excel_2013_logo.svg/1043px-Microsoft_Excel_2013_logo.svg.png"
                 alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Selecione seu arquivo!</h1>


            <div class="form-group">
                <label for="inputXLSX" class="sr-only">Anexe aqui seu XLSX</label>
                <input type="file" id="inputXLSX" name="file" class="form-control form-control-sm"
                       placeholder="XLSX arquivo" required="required">
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit"> Carregar arquivo</button>
            <p class="mt-5 mb-3 text-muted">© Luiz Resplande</p>
        </form>
    </div>
@endsection


