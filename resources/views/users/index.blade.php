@extends('template.template')

@section('content')
    <div class="row mt-5">
        <div class="">
        <a href="/"><button type="button" class="btn btn-success">Atualizar Lista</button></a>
        </div>
    <table class="table table-bordered mt-5">
        <thead>
        <tr>
            <th scope="col">Dados</th>
            <th scope="col">Nome</th>
            <th scope="col">Documento</th>
            <th scope="col">Email</th>
            <th scope="col">Cep</th>
            <th scope="col">Telefone</th>
            <th scope="col">Ativo</th>

        </tr>
        </thead>
        <tbody>

        @foreach($users as $user)
        <tr>
            <td><a href="/users/{{$user->id}}">Detalhes</a></td>
            <td>{{ $user->nome }}</td>
            <td>{{ $user->documento }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->cep }}</td>
            <td>{{ $user->telefone }}</td>
            <td>{{ $user->ativo }}</td>
        </tr>
        @endforeach

        </tbody>
    </table>
        {{ $users->links() }}
    </div>
@endsection


