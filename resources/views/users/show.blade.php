@extends('template.template')

@section('content')
    <div class="row mt-5">
        <div class="col-12">
            <div class="card">

                <div class="card-body">
                    <div class="card-title mb-4">
                        <div class="d-flex justify-content-start">

                            <div class="userData ml-3">
                                <h2 class="d-block" style="font-size: 1.5rem; font-weight: bold"><a href="javascript:void(0);">{{$user->nome}}</a></h2>

                            </div>
                            <div class="ml-auto">
                                <input type="button" class="btn btn-primary d-none" id="btnDiscard" value="Discard Changes" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs mb-4" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="basicInfo-tab" data-toggle="tab" href="#basicInfo" role="tab" aria-controls="basicInfo" aria-selected="true">Informações</a>
                                </li>

                            </ul>
                            <div class="tab-content ml-1" id="myTabContent">
                                <div class="tab-pane fade show active" id="basicInfo" role="tabpanel" aria-labelledby="basicInfo-tab">


                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">E-mail</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            {{ $user->email }}
                                        </div>
                                    </div>
                                    <hr />


                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Documento</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            {{ $user->documento }}
                                        </div>
                                    </div>
                                    <hr />


                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">CEP</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            {{ $user->cep }}
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Endereco</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            {{ $user->endereco }}
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Bairro</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            {{ $user->bairro }}
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Cidade</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            {{ $user->cidade }}
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">UF</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            {{ $user->uf }}
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                        <div class="col-sm-3 col-md-2 col-5">
                                            <label style="font-weight:bold;">Telefone</label>
                                        </div>
                                        <div class="col-md-8 col-6">
                                            {{ $user->telefone }}
                                        </div>
                                    </div>
                                    <hr />

                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="text-left">
                        <a href="javascript:history.go(-1) "><button type="button" class="btn btn-primary">Voltar</button></a>
                    </div>
                </div>


            </div>

        </div>

    </div>
@endsection