@if(Session::has('message'))
    <p class="alert alert-success mt-3">{{ Session::get('message') }}</p>
@endif

@if(Session::has('error'))
    <p class="alert alert-danger mt-3">{{ Session::get('error') }}</p>
    @endif
</body>
</html>